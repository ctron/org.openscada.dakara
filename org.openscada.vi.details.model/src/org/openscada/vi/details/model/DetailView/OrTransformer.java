/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Or Transformer</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.vi.details.model.DetailView.DetailViewPackage#getOrTransformer()
 * @model
 * @generated
 */
public interface OrTransformer extends CompositeTransformer
{
} // OrTransformer
