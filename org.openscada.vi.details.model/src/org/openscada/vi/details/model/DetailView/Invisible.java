/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Invisible</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An implementation that is always invisible.
 * <!-- end-model-doc -->
 *
 *
 * @see org.openscada.vi.details.model.DetailView.DetailViewPackage#getInvisible()
 * @model
 * @generated
 */
public interface Invisible extends Visibility
{
} // Invisible
