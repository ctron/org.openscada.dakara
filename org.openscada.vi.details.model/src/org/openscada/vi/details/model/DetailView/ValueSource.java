/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Source</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.vi.details.model.DetailView.DetailViewPackage#getValueSource()
 * @model abstract="true"
 * @generated
 */
public interface ValueSource extends EObject
{
} // ValueSource
