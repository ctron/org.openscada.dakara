/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fill Layout Component</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.vi.details.model.DetailView.DetailViewPackage#getFillLayoutComponent()
 * @model
 * @generated
 */
public interface FillLayoutComponent extends CompositeComponent
{
} // FillLayoutComponent
