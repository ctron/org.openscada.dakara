/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>And Transformer</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.vi.details.model.DetailView.DetailViewPackage#getAndTransformer()
 * @model
 * @generated
 */
public interface AndTransformer extends CompositeTransformer
{
} // AndTransformer
