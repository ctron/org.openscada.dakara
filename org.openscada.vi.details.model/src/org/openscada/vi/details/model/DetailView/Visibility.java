/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visibility</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.vi.details.model.DetailView.DetailViewPackage#getVisibility()
 * @model abstract="true"
 * @generated
 */
public interface Visibility extends EObject
{
} // Visibility
