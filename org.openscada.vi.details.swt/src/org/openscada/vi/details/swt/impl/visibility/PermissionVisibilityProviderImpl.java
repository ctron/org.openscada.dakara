/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jürgen Rose (cptmauli@googlemail.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.vi.details.swt.impl.visibility;

import java.util.Arrays;
import java.util.List;

import org.openscada.core.ui.connection.login.LoginSession;
import org.openscada.core.ui.connection.login.SessionListener;
import org.openscada.core.ui.connection.login.SessionManager;

public class PermissionVisibilityProviderImpl extends AbstractVisibilityProvider implements SessionListener
{

    private final List<String> permissions;

    public PermissionVisibilityProviderImpl ( final String permissions )
    {
        this.permissions = Arrays.asList ( permissions.split ( "[,|]" ) );
        SessionManager.getDefault ().addListener ( this );
    }

    protected boolean evalPermission ()
    {
        boolean result = false;
        for ( String permission : this.permissions )
        {
            if ( permission.startsWith ( "!" ) )
            {
                result |= !SessionManager.getDefault ().hasRole ( permission.substring ( 1 ) );
            }
            else
            {
                result |= SessionManager.getDefault ().hasRole ( permission );
            }
        }
        return result;
    }

    @Override
    public void dispose ()
    {
        SessionManager.getDefault ().removeListener ( this );
    }

    @Override
    public void sessionChanged ( final LoginSession session )
    {
        fireChange ( evalPermission () );
    }

}
