/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.vi.details.swt.impl.visibility;

import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.swt.widgets.Control;
import org.openscada.vi.data.SummaryProvider;
import org.openscada.vi.details.swt.impl.GroupTab;
import org.openscada.vi.details.swt.impl.tab.TabProvider.Folder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A visibility component for TabFolders
 * <p>
 * The provided control will be disposed when this component is disposed.
 * </p>
 * 
 * @author Jens Reimann
 */
public class TabVisibleComponent implements VisibleComponent
{

    private final static Logger logger = LoggerFactory.getLogger ( TabVisibleComponent.class );

    private final Folder folder;

    private final Control control;

    private final GroupTab groupTab;

    public TabVisibleComponent ( final Folder folder, final int index, final GroupTab groupTab, final Control control )
    {
        this.folder = folder;
        this.control = control;
        this.groupTab = groupTab;
    }

    @Override
    public void show ()
    {
        logger.debug ( "Showing tab ... " );

        this.folder.setVisible ( true );
    }

    @Override
    public void hide ()
    {
        this.folder.setVisible ( false );
    }

    @Override
    public void create ()
    {
        show ();
    }

    @Override
    public void dispose ()
    {
        hide ();
        this.folder.dispose ();
        this.control.dispose ();
    }

    @Override
    public IObservableSet getDescriptors ()
    {
        return this.groupTab.getDescriptors ();
    }

    @Override
    public void start ()
    {
        this.groupTab.start ();
    }

    @Override
    public void stop ()
    {
        this.groupTab.stop ();
    }

    @Override
    public SummaryProvider getSummaryProvider ()
    {
        return this.groupTab.getSummaryProvider ();
    }
}
