package org.openscada.vi.details.swt.widgets;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.openscada.vi.data.DataValue;
import org.openscada.vi.data.RegistrationManager;
import org.openscada.vi.data.RegistrationManager.Listener;
import org.openscada.vi.data.SummaryInformation;
import org.openscada.vi.data.SummaryListener;
import org.openscada.vi.data.SummaryProvider;

public class RegistrationManagerSummaryProvider implements SummaryProvider, Listener
{

    private final RegistrationManager registrationManager;

    private SummaryInformation summaryInformation;

    private final Set<SummaryListener> listeners = new CopyOnWriteArraySet<SummaryListener> ();

    private boolean closed;

    public RegistrationManagerSummaryProvider ( final RegistrationManager registrationManager )
    {
        this.registrationManager = registrationManager;
        registrationManager.addListener ( this );
        triggerDataUpdate ();
    }

    public void dispose ()
    {
        this.registrationManager.removeListener ( this );
    }

    @Override
    public SummaryInformation getSummary ()
    {
        return this.summaryInformation;
    }

    @Override
    public void addSummaryListener ( final SummaryListener summaryListener )
    {
        if ( this.listeners.add ( summaryListener ) )
        {
            summaryListener.summaryChanged ( this.summaryInformation );
        }
    }

    @Override
    public void removeSummaryListener ( final SummaryListener summaryListener )
    {
        this.listeners.remove ( summaryListener );
    }

    @Override
    public void triggerDataUpdate ()
    {
        this.summaryInformation = new SummaryInformation ( this.registrationManager.getData () );

        final SummaryInformation si = this.closed ? new SummaryInformation ( Collections.<String, DataValue> emptyMap () ) : this.summaryInformation;

        for ( final SummaryListener listener : this.listeners )
        {
            listener.summaryChanged ( si );
        }
    }

    public void open ()
    {
        this.closed = false;
        triggerDataUpdate ();
    }

    public void close ()
    {
        this.closed = true;
        triggerDataUpdate ();
    }
}
