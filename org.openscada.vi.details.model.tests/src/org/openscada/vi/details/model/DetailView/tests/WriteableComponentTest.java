/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView.tests;

import org.openscada.vi.details.model.DetailView.WriteableComponent;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Writeable Component</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class WriteableComponentTest extends ReadableComponentTest
{

    /**
     * Constructs a new Writeable Component test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public WriteableComponentTest ( String name )
    {
        super ( name );
    }

    /**
     * Returns the fixture for this Writeable Component test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected WriteableComponent getFixture ()
    {
        return (WriteableComponent)fixture;
    }

} //WriteableComponentTest
