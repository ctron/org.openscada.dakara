/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView.tests;

import junit.framework.TestCase;

import org.openscada.vi.details.model.DetailView.Visibility;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Visibility</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class VisibilityTest extends TestCase
{

    /**
     * The fixture for this Visibility test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected Visibility fixture = null;

    /**
     * Constructs a new Visibility test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public VisibilityTest ( String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Visibility test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( Visibility fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Visibility test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected Visibility getFixture ()
    {
        return fixture;
    }

} //VisibilityTest
