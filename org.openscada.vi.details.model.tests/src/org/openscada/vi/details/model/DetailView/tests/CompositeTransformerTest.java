/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView.tests;

import org.openscada.vi.details.model.DetailView.CompositeTransformer;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Composite Transformer</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CompositeTransformerTest extends ValueSourceTest
{

    /**
     * Constructs a new Composite Transformer test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CompositeTransformerTest ( String name )
    {
        super ( name );
    }

    /**
     * Returns the fixture for this Composite Transformer test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected CompositeTransformer getFixture ()
    {
        return (CompositeTransformer)fixture;
    }

} //CompositeTransformerTest
