/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView.tests;

import org.openscada.vi.details.model.DetailView.CompositeComponent;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Composite Component</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CompositeComponentTest extends ComponentTest
{

    /**
     * Constructs a new Composite Component test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CompositeComponentTest ( String name )
    {
        super ( name );
    }

    /**
     * Returns the fixture for this Composite Component test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected CompositeComponent getFixture ()
    {
        return (CompositeComponent)fixture;
    }

} //CompositeComponentTest
