/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView.tests;

import org.openscada.vi.details.model.DetailView.GenericComponent;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Generic Component</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class GenericComponentTest extends ComponentTest
{

    /**
     * Constructs a new Generic Component test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public GenericComponentTest ( String name )
    {
        super ( name );
    }

    /**
     * Returns the fixture for this Generic Component test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected GenericComponent getFixture ()
    {
        return (GenericComponent)fixture;
    }

} //GenericComponentTest
