/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView.tests;

import org.openscada.vi.details.model.DetailView.ReadableComponent;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Readable Component</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ReadableComponentTest extends ComponentTest
{

    /**
     * Constructs a new Readable Component test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ReadableComponentTest ( String name )
    {
        super ( name );
    }

    /**
     * Returns the fixture for this Readable Component test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected ReadableComponent getFixture ()
    {
        return (ReadableComponent)fixture;
    }

} //ReadableComponentTest
