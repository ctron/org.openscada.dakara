/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.vi.details.model.DetailView.tests;

import junit.framework.TestCase;

import org.openscada.vi.details.model.DetailView.ValueSource;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Value Source</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ValueSourceTest extends TestCase
{

    /**
     * The fixture for this Value Source test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ValueSource fixture = null;

    /**
     * Constructs a new Value Source test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ValueSourceTest ( String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Value Source test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( ValueSource fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Value Source test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ValueSource getFixture ()
    {
        return fixture;
    }

} //ValueSourceTest
