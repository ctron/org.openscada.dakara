/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.vi.ui.viewer.ext.item;

import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.widgets.Composite;
import org.openscada.ui.databinding.item.VariantToStringConverter;

public class PlainDataItemLabel extends DataItemLabel
{

    public PlainDataItemLabel ( final Composite composite, final String connectionId, final String itemId )
    {
        super ( composite, connectionId, itemId );
    }

    @Override
    protected void bind ()
    {
        this.dbc.bindValue ( SWTObservables.observeText ( this.label ), this.model, null, new UpdateValueStrategy ().setConverter ( new VariantToStringConverter () ) );
        this.model.addValueChangeListener ( new IValueChangeListener () {

            @Override
            public void handleValueChange ( final ValueChangeEvent event )
            {
                if ( !PlainDataItemLabel.this.label.isDisposed () )
                {
                    PlainDataItemLabel.this.label.getParent ().layout ();
                }
            }
        } );
    }

}
