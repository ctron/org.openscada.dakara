/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2006-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.vi.ui.viewer.ext.item;

import java.util.Hashtable;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;

public abstract class AbstractItemExtension implements IExecutableExtension
{

    protected String connectionId;

    protected String itemId;

    @Override
    public void setInitializationData ( final IConfigurationElement config, final String propertyName, final Object data ) throws CoreException
    {

        if ( data instanceof String )
        {
            final String str = (String)data;
            if ( str.contains ( "#" ) )
            {
                final String[] tok = str.split ( "\\#", 2 );
                this.connectionId = tok[0];
                this.itemId = tok[1];
            }
        }
        if ( data instanceof Hashtable<?, ?> )
        {
            final Hashtable<?, ?> props = (Hashtable<?, ?>)data;

            this.connectionId = "" + props.get ( "connectionId" );
            this.itemId = "" + props.get ( "itemId" );
        }

    }
}
