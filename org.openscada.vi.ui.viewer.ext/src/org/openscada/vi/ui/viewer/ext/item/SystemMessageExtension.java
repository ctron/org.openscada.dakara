/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.vi.ui.viewer.ext.item;

import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.openscada.core.Variant;
import org.openscada.vi.ui.user.viewer.ViewManager;
import org.openscada.vi.ui.user.viewer.ext.ViewerExtension;

public class SystemMessageExtension extends AbstractItemExtension implements ViewerExtension
{

    public static class SystemMessageLabel extends DataItemLabel
    {
        private final LocalResourceManager resourceManager;

        public SystemMessageLabel ( final Composite parent, final String connectionId, final String itemId )
        {
            super ( parent, connectionId, itemId );
            this.resourceManager = new LocalResourceManager ( JFaceResources.getResources ( parent.getDisplay () ), this.label );
        }

        @Override
        protected void bind ()
        {
            this.model.addValueChangeListener ( new IValueChangeListener () {

                @Override
                public void handleValueChange ( final ValueChangeEvent event )
                {
                    if ( !SystemMessageLabel.this.label.isDisposed () )
                    {
                        process ( event.diff.getNewValue () );
                    }
                }
            } );
        }

        protected void process ( final Object value )
        {
            String text = "";

            if ( value instanceof Variant )
            {
                text = ( (Variant)value ).asString ( "" );
            }
            final int idx = text.indexOf ( "#" );

            String icon = null;
            if ( idx >= 0 && idx < text.length () )
            {
                icon = text.substring ( 0, idx );
                text = text.substring ( idx + 1 );
            }

            this.label.setText ( text );

            if ( icon == null )
            {
                this.label.setImage ( null );
            }
            else
            {
                this.label.setImage ( this.resourceManager.createImageWithDefault ( makeDescriptor ( icon ) ) );
            }

            this.label.getParent ().layout ();
        }

        private ImageDescriptor makeDescriptor ( final String icon )
        {
            if ( "info".equalsIgnoreCase ( icon ) )
            {
                return ImageDescriptor.createFromFile ( SystemMessageExtension.class, "icons/information.png" );
            }
            else if ( "warning".equalsIgnoreCase ( icon ) )
            {
                return ImageDescriptor.createFromFile ( SystemMessageExtension.class, "icons/exclamation.png" );
            }

            return ImageDescriptor.getMissingImageDescriptor ();
        }
    }

    @Override
    public Control create ( final Composite parent, final ViewManager viewManager, final boolean horizontal )
    {
        return new SystemMessageLabel ( parent, this.connectionId, this.itemId ).getControl ();
    }

}
